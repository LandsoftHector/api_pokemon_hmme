package com.example.hmme_poke_exa.model.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "pokemon")
data class PokeSave(
    @ColumnInfo(name = "pokename")
    var pokename: String,

    @ColumnInfo(name = "url")
    var url: String,

)

{
    constructor() : this("","")

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var Id: Int? = null
}