package com.example.hmme_poke_exa.ui.poke

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.hmme_poke_exa.model.Pokemon
import com.example.hmme_poke_exa.model.room.PokeSave
import com.example.hmme_poke_exa.repository.PokeRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PokeViewModel constructor(private val repository: PokeRepository)  : ViewModel() {

    var liveDataPoke: LiveData<PokeSave>? = null // room
    val pokeList = MutableLiveData<Pokemon>()
    val errorMessage = MutableLiveData<String>()

    fun getAllPokemon() {

        val response = repository.getAllMovies()
        response.enqueue(object : Callback<Pokemon> {
            override fun onResponse(call: Call<Pokemon>, response: Response<Pokemon>) {
                if(response.isSuccessful){
                    pokeList.postValue(response.body())
                }
            }

            override fun onFailure(call: Call<Pokemon>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }

    fun insertData(context: Context, username: String, password: String) {
        PokeRepository.insertData(context, username, password)
    }
}