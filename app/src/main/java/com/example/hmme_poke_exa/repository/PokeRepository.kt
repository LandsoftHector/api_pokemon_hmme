package com.example.hmme_poke_exa.repository

import android.content.Context
import com.example.hmme_poke_exa.model.room.PokeSave
import com.example.hmme_poke_exa.network.RetrofitService
import com.example.hmme_poke_exa.network.room.DataBasePoke
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PokeRepository constructor(private val retrofitService: RetrofitService) {
    fun getAllMovies() = retrofitService.getAllMovies()

    companion object{
        var pokeDatabase: DataBasePoke? = null

        fun initializeDB(context: Context) : DataBasePoke {
            return DataBasePoke.getDataseClient(context)
        }

        fun insertData(context: Context, username: String, password: String) {

            pokeDatabase = initializeDB(context)

            CoroutineScope(Dispatchers.IO).launch {
                val pokeDetails = PokeSave(username, password)
                pokeDatabase!!.pokeDao().InsertData(pokeDetails)
            }

        }
    }

}