package com.example.hmme_poke_exa.network.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.example.hmme_poke_exa.model.PokeList
import com.example.hmme_poke_exa.model.room.PokeSave

@Dao
interface DAOPke {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun InsertData(pokeTableModel: PokeSave)

}