package com.example.hmme_poke_exa.network.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.hmme_poke_exa.model.room.PokeSave

@Database(entities = arrayOf(PokeSave::class), version = 4, exportSchema = false)
 abstract  class DataBasePoke : RoomDatabase(){

    abstract fun pokeDao() : DAOPke

    companion object {

        @Volatile
        private var INSTANCE: DataBasePoke? = null

        fun getDataseClient(context: Context) : DataBasePoke {

            if (INSTANCE != null) return INSTANCE!!

            synchronized(this) {

                INSTANCE = Room
                    .databaseBuilder(context, DataBasePoke::class.java, "POKE_DATABASE")
                    .fallbackToDestructiveMigration()
                    .build()

                return INSTANCE!!

            }
        }

    }
}
